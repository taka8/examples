package taka8.examples.httpclient;

import java.net.CookieManager;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpClientTest {

	private static final Logger __logger = LoggerFactory.getLogger(HttpClientTest.class);

	@Test
	public void testSample() throws Exception {
		var client = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).connectTimeout(Duration.ofSeconds(20))
				.cookieHandler(new CookieManager()).build();
		HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create("https://www.google.com")).build();
		var response = client.send(request, HttpResponse.BodyHandlers.ofString());
		__logger.info("response=<{}>", response);
		__logger.info("body=<{}>", response.body());
	}

}
