package taka8.example.springboot;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
@RestController
public class Application {

	private static final Logger __logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Value("${server.port}")
	private int _port;

	@Autowired
	private DummyConfig _config;

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		_config.getValueMap().forEach((k, v) -> {
			__logger.info("key=<{}> keyType=<{}> value=<{}> valueType=<{}>", k, k.getClass(), v, v.getClass());
		});
		var input = _convertMap(_config.getValueMap());
		var om = new ObjectMapper();
		var config = om.convertValue(input, _Config.class);
		__logger.info("config=<{}>", config);
		return "Hello port=<" + _port + ">";
	}

	private Map<String, Object> _convertMap(Map<String, Object> valueMap) {
		Map<String, Object> ret = new LinkedHashMap<>();
		valueMap.forEach((k, v) -> {
			ret.put(k, _convertValue(v));
		});
		return ret;
	}

	@SuppressWarnings("unchecked")
	private Object _convertValue(Object value) {
		if (value instanceof Map) {
			var m = (Map<String, Object>) value;
			boolean isArray = true;
			for (var k : m.keySet()) {
				if (!_isNumber(k)) {
					isArray = false;
				}
			}
			if (isArray) {
				List<Object> ret = new ArrayList<>();
				m.values().forEach(v -> {
					ret.add(_convertValue(v));
				});
				return ret;

			} else {
				return _convertMap(m);
			}
		} else {
			return value;
		}
	}

	private boolean _isNumber(String label) {
		for (var c : label.toCharArray()) {
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}

	private static class _Config {

		@JsonProperty("stringProp")
		private String _stringProp;

		@JsonProperty("intProp")
		private int _intProp;

		@JsonProperty("arrayStringProp")
		private List<String> _arrayStringProp;

		@JsonProperty("arrayIntProp")
		private List<Integer> _arrayIntProp;

		@Override
		public String toString() {
			return this.getClass().getSimpleName() + "/[stringProp=" + _stringProp + ",intProp=" + _intProp
					+ ",arrayStringProp=" + _arrayStringProp + ",arrayIntProp=" + _arrayIntProp + "]";
		}

	}
}
