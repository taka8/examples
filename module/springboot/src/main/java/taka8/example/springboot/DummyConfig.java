package taka8.example.springboot;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "dummy")
public class DummyConfig {

	private Map<String, Object> _valueMap;

	public Map<String, Object> getValueMap() {
		return _valueMap;
	}

	public void setValueMap(Map<String, Object> valueMap) {
		_valueMap = valueMap;
	}

}
